# misc

## curses.py.patch

patch for trackma v0.8.2 to sort newer shows at the top

apply to ~/.local/lib/python3.7/site-packages/trackma/ui/curses.py

## simple-remote-access.sh

playing around with ncat, made a simple program to execute commands through web browser
