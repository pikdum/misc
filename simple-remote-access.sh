#!/usr/bin/env bash

# Usage: ncat -e ./simple-remote-access.sh -kl 8888

set -euo pipefail

read -r method path _

case $method in
    GET)
        printf 'HTTP/1.1 200 OK\r\n\r\n'
        echo '<form method="POST" action="/">'
        echo '<input type="text" name="cmd" id="cmd">'
        echo '<input type=submit value=Run onclick=this.form.action=document.getElementById("cmd").value>'
        echo '</form>'
        ;;
    POST)
        ${path:1} &> /dev/null &
        printf 'HTTP/1.1 302 Found\r\nLocation: /\r\n\r\n'
        ;;
esac
